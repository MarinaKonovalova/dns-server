from Header import Header
from Query import Query
from ResourceRecord import ResourceRecord
from math import nan
from collections import defaultdict


class Request:
    def __init__(self, data: bytes):
        self.data = data
        self.header = Header(self.data)
        self.queries = self.parse_queries()
        self.answers = self.parse_answers()
        self.additional_records = self.parse_additional_records()

    @classmethod
    def from_records(cls, queries: list, answers: list, header: Header, additional_records: list):
        obj = object.__new__(cls)
        obj.__dict__["header"] = header
        obj.__dict__["queries"] = queries
        obj.__dict__["answers"] = answers
        obj.__dict__["additional_records"] = additional_records
        data = bytearray()
        data.extend(header.to_bytes())
        for query in queries:
            data.extend(query.to_bytes())
        cls.append_records(answers, data, queries)
        cls.append_records(additional_records, data, queries)
        #TODO: additional records
        obj.__dict__["data"] = data
        return obj

    @classmethod
    def append_records(cls, records, data, queries):
        query_to_records = defaultdict(ResourceRecordItems)
        for query in queries:
            query_offset = 12
            for record in records:
                if record.NAME == query.QNAME:
                    query_to_records[query].items.append(record)
                    query_to_records[query].offset = query_offset
            query_offset += query.total_length
        for key, value in query_to_records.items():
            for record in value.items:
                data.extend(record.to_bytes(value.offset))

    def to_bytes(self):
        return self.data

    def parse_queries(self):
        queries = []
        offset = self.header.total_length
        for i in range(self.header.QDCOUNT):
            queries.append(Query(self.data, offset))
            offset += queries[-1].total_length
        return queries

    def parse_answers(self):
        answers = []
        offset = sum([x.total_length for x in self.queries]) + self.header.total_length
        for i in range(0, self.header.ANCOUNT):
            answers.append(ResourceRecord(self.data, offset))
            offset += answers[-1].total_length
        return answers

    def parse_additional_records(self):
        additional_records = []
        offset = sum([x.total_length for x in self.queries]) + \
            sum([x.total_length for x in self.answers]) + self.header.total_length

        for i in range(0, self.header.ARCOUNT):
            additional_records.append(ResourceRecord(self.data, offset))
            offset += additional_records[-1].total_length
        return additional_records


class ResourceRecordItems:
    def __init__(self, offset=nan):
        self.offset = offset
        self.items = []
