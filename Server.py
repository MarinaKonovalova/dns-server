import socket
from Request import Request
from Repository import Repository
from Request import Header
import copy
from StoppableThread import StoppableThread


class Server(StoppableThread):
    def __init__(self, host, port, repository: Repository):
        StoppableThread.__init__(self)
        self.host = host
        self.port = port
        self.repository = repository

    def run(self):
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
            s.bind((self.host, self.port))
            s.setblocking(False)
            print("Server has started")
            while True:
                if self.stopped():
                    self.repository._serialize()
                    print("Server has stopped")
                    return
                try:
                    data, addr = s.recvfrom(512)
                    request = Request(data)
                    suitable_records = [self.repository.get_by_query(x) for x in request.queries]
                    for query_record in suitable_records:
                        if not len(query_record):
                            self.ask_external_server(request)
                    suitable_records = [self.repository.get_by_query(x) for x in request.queries]
                    response = self.create_response(request.header.ID,
                                                    [answer for answers in suitable_records for answer in answers],
                                                    request.header, request.queries)
                    s.sendto(response.to_bytes(), addr)
                except socket.error:
                    continue

    @staticmethod
    def create_response(transaction_id: int, answers: list, old_header: Header, queries: list):
        old_header.QR = 1
        old_header.ANCOUNT = len(answers)
        old_header.ID = transaction_id
        old_header.ARCOUNT = 0
        return Request.from_records(queries, answers, old_header, [])

    def ask_external_server(self, request: Request):
        request_copy = copy.deepcopy(request)
        for query in request_copy.queries:
            query.QTYPE = 255
        try:
            with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as client_socket:
                client_socket.settimeout(1)
                client_socket.sendto(request_copy.to_bytes(), ('8.8.8.8', 53))
                data, _ = client_socket.recvfrom(512)
                response = Request(data)
                self.update_cache(response.answers)
        except OSError:
            pass

    def update_cache(self, records: list):
        for record in records:
            self.repository.add_or_update(record)
