from CacheCleanerThread import CacheCleanerThread
from Repository import Repository
from Server import Server
from threading import Lock
if __name__ == '__main__':
    lock = Lock()
    repository = Repository(lock)
    server_thread = Server('127.0.0.1', 53, repository)
    server_thread.start()

    cleaner_thread = CacheCleanerThread(repository)
    cleaner_thread.start()

    command = input()
    if command == "stop":
        server_thread.stop()
        cleaner_thread.stop()
