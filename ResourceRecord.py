from Query import Query
import base64
import struct


class ResourceRecord:
    def __init__(self, data: bytes, offset: int):
        self.offset = offset
        self.data = data[offset:]
        self.NAME, _ = self._get_name_from_pointer(self.data[0:2], data)
        self.TYPE = int.from_bytes(self.data[2:4], 'big')
        self.CLASS = int.from_bytes(self.data[4:6], 'big')
        self.TTL = int.from_bytes(self.data[6:10], 'big')
        self.RDLENGTH = int.from_bytes(self.data[10:12], 'big')
        self.RDATA = self.data[12: 12 + self.RDLENGTH]
        self.total_length = self.RDLENGTH + 12

    @classmethod
    def from_line(cls, line: str):
        entires = line.split()
        obj = object.__new__(cls)
        obj.__dict__["NAME"] = [bytes(x, encoding="utf8") for x in entires[0].split('.')]
        obj.__dict__["TYPE"] = int(entires[1])
        obj.__dict__["CLASS"] = int(entires[2])
        obj.__dict__["TTL"] = int(entires[3])
        obj.__dict__["RDLENGTH"] = int(entires[4])
        obj.__dict__["RDATA"] = base64.b64decode(entires[5])
        obj.__dict__["total_length"] = obj.RDLENGTH + 12
        return obj

    def to_bytes(self, name_pointer):
        pointer = name_pointer.to_bytes(2, byteorder="big")
        int_pointer = int.from_bytes([pointer[0] | 0b11000000, pointer[1]], byteorder="big")
        res = bytearray(struct.pack(">HHHIH", int_pointer, self.TYPE,
                                    self.CLASS, self.TTL, self.RDLENGTH))
        res.extend(self.RDATA)
        return res

    def _get_name_from_pointer(self, pointer: bytes, full_data: bytes):
        offset = int.from_bytes(bytes([pointer[0] & 0b00111111, pointer[1]]),
                                byteorder='big')
        return Query.parse_domain(full_data[offset:])
