import struct
from bitarray import bitarray
import bitstring
class Header:
    def __init__(self, data: bytes):
        self.ID = int.from_bytes(data[0:2], 'big')
        self.QR = int(data[2] &     0b1)
        self.OPCODE = int(data[2] & 0b01111000)
        self.AA = int(data[2] &     0b00000100)
        self.TC = int(data[2] &     0b00000010)
        self.RD = int(data[2] &     0b00000001)

        self.RA = int(data[3] &     0b10000000)
        self.Z = int(data[3] &      0b01000000)
        self.RCODE = int(data[3] &  0b00001111)

        self.QDCOUNT = int.from_bytes(data[4:6], 'big')
        self.ANCOUNT = int.from_bytes(data[6:8], 'big')
        self.NSCOUNT = int.from_bytes(data[8:10], 'big')
        self.ARCOUNT = int.from_bytes(data[10:12], 'big')
        self.total_length = 12

    # TODO: check rd ra z
    def to_bytes(self) -> bytes:
        return struct.pack(">HBBHHHH", self.ID,
                          self.create_byte([(self.QR, 1), (self.OPCODE, 4), (self.AA, 1), (self.TC, 1), (self.RD, 1)]),
                          self.create_byte([(self.RA, 1), (self.Z, 3), (self.RCODE, 4)]),
                          self.QDCOUNT,
                          self.ANCOUNT,
                          self.NSCOUNT,
                          self.ARCOUNT)

    def create_byte(self, values: list):
        byte = bitarray()
        for value, length in values:
            as_bits = format(value, f"0{length}b")
            byte.extend(bitarray(as_bits))
        return int(byte.to01(), 2)


