class Query:
    def __init__(self, data: bytes, offset):
        self.offset = offset
        self.data = data[self.offset:]
        self.QNAME, self.OFFSET = self.parse_domain(self.data)
        self.QTYPE = int.from_bytes(self.data[self.OFFSET + 1: self.OFFSET + 3], 'big')
        self.QCLASS = int.from_bytes(self.data[self.OFFSET + 3: self.OFFSET + 5], 'big')
        self.total_length = self.OFFSET + 5

    def to_bytes(self):
        result_data = bytearray()
        result_data.extend(self.data[:self.OFFSET + 1])
        result_data.extend(self.QTYPE.to_bytes(2, 'big'))
        result_data.extend(self.QCLASS.to_bytes(2, 'big'))
        return result_data

    @staticmethod
    def parse_domain(data):
        prev_position = 0
        next_position = data[0]
        domain = []
        offset = 0
        while next_position != 0:
            offset = prev_position + next_position + 1
            domain.append(data[prev_position + 1:offset])
            prev_position = offset
            next_position = data[offset]
        return domain, offset
