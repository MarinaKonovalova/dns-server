from StoppableThread import StoppableThread
import time
from Repository import Repository


class CacheCleanerThread(StoppableThread):

    def __init__(self, repository: Repository):
        StoppableThread.__init__(self)
        self.repository = repository
        pass

    def run(self):
        while True:
            if self.stopped():
                print("Cleaner has stopped")
                break
            self.repository._clean_old_records()
            time.sleep(20)
