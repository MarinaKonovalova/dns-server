from ResourceRecord import ResourceRecord
import base64
from Query import Query
import datetime
import time
from collections import defaultdict
from itertools import filterfalse
from threading import Lock


class Repository:
    def __init__(self, lock: Lock):
        self.filename = "cache.txt"
        self.data = self._read_from_file()
        self.lock = lock

    def get_by_query(self, query: Query) -> list:
        domain_as_string = self._domain_to_str(query.QNAME)
        records = self.data[domain_as_string]
        return [x[0] for x in records if
                x[0].TYPE == query.QTYPE and
                x[0].CLASS == query.QCLASS and self.TTL_is_valid(x[1], x[0].TTL)]

    def add_or_update(self, item: ResourceRecord):
        domain_as_string = self._domain_to_str(item.NAME)
        for idx, (record, created_at) in enumerate(self.data[domain_as_string]):
            if record.TYPE == item.TYPE and record.NAME == item.NAME and record.RDATA == item.RDATA:
                if self.TTL_is_valid(created_at, record.TTL):
                    return
                with self.lock:
                    self.data[domain_as_string][idx] = (item, time.time())
                return
        with self.lock:
            self.data[domain_as_string].append((item, time.time()))

    def _serialize(self):
        with open(self.filename, "a") as f:
            for domain, records in self.data.items():
                for record in filterfalse(lambda x: not self.TTL_is_valid(x[1], x[0].TTL), records):
                    rdata = str(base64.b64encode(record[0].RDATA), encoding='utf8')
                    line = f"{domain} {record[0].TYPE} {record[0].CLASS} {record[0].TTL} " \
                           f"{record[0].RDLENGTH} {rdata} {record[1]}\n"
                    f.write(line)

    def _read_from_file(self):
        with open(self.filename, "r") as f:
            lines = f.readlines()

        with open(self.filename, "w") as f:
            records = defaultdict(list)
            for line in lines:
                created_at = line.split()[-1]
                TTL = int(line.split()[3])
                if self.TTL_is_valid(created_at, TTL):
                    f.write(line)
                    record = ResourceRecord.from_line(line)
                    domain_as_string = self._domain_to_str(record.NAME)
                    records[domain_as_string].append((record, created_at))
            return records

    def _clean_old_records(self):
        for item in self.data.items():
            for record in item[1]:
                if not self.TTL_is_valid(record[1], record[0].TTL):
                    with self.lock:
                        item[1].remove(record)

    @staticmethod
    def TTL_is_valid(created_at: str, TTL):
        created_at = datetime.datetime.fromtimestamp(float(created_at))
        return created_at + datetime.timedelta(seconds=TTL) >= datetime.datetime.now()

    @staticmethod
    def _domain_to_str(domain: list):
        return ".".join([str(x, encoding='utf8') for x in domain])